---
license: mit
---
### matchair on Stable Diffusion via Dreambooth
#### model by mattoescobar
This your the Stable Diffusion model fine-tuned the matchair concept taught to Stable Diffusion with Dreambooth.
It can be used by modifying the `instance_prompt`: **a photo of matchair**

You can also train your own concepts and upload them to the library by using [this notebook](https://colab.research.google.com/github/huggingface/notebooks/blob/main/diffusers/sd_dreambooth_training.ipynb).
And you can run your new concept via `diffusers`: [Colab Notebook for Inference](https://colab.research.google.com/github/huggingface/notebooks/blob/main/diffusers/sd_dreambooth_inference.ipynb), [Spaces with the Public Concepts loaded](https://huggingface.co/spaces/sd-dreambooth-library/stable-diffusion-dreambooth-concepts)

Here are the images used for training this concept:
![image 0](https://huggingface.co/mattoescobar/matchair/resolve/main/concept_images/IMG_20221018_202150341.jpeg)
![image 1](https://huggingface.co/mattoescobar/matchair/resolve/main/concept_images/IMG_20221018_202250452.jpeg)
![image 2](https://huggingface.co/mattoescobar/matchair/resolve/main/concept_images/IMG_20221018_202206002.jpeg)
![image 3](https://huggingface.co/mattoescobar/matchair/resolve/main/concept_images/IMG_20221018_202256982.jpeg)
![image 4](https://huggingface.co/mattoescobar/matchair/resolve/main/concept_images/IMG_20221018_202242170.jpeg)

Also, regarding computational power, I managed to run this script using the lightest Nvidia Tesla V100 server option in the platform. 
